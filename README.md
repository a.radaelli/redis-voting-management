# Redis - Voting Management

## First things first

This project is the result of a Redis course I took during my associate's degree.
Thanks to this experience I learnt:
- how key-value data structures work and how to implement a programming logic on its characteristics
- Redis documentation
- DB communication between Python and Redis

## Description

This project needs to able to manage a voting system of group of people.
the key requirements are:
- every individual is classified as unique and need to login the system to cast a vote
- an individual can vote for all of the proposals but just once
- an individual can write a proposal for the other to be voted
- the program needs to print back the proposals from the highest voted proposal to the lowest
- all the updates must be saved in a Redis DB, so that next time someone starts the program, all the data are updated to the lastest version
