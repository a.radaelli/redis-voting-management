import redis


class RedisDB():
    def __init__(self):
        '''connection to Redis Server'''
        try:
            self.r = redis.Redis(host='localhost', port=6379, db=0, charset='utf-8', decode_responses=True)
        except Exception as e:
            print('Error, can\'t connect to Redis Server')
            print(e)

    def _login(self):
        '''user login'''
        try:
            print('LOGIN:')
            while True:
                nome = input('  Username: ')
                if nome == '':
                    print('  Invalid username')
                else:
                    break
            while True:
                cognome = input('  Surname: ')
                if cognome == '':
                    print('  Invalid surname')
                else:
                    break
            self.user = nome.lower() + '_' + cognome.lower()
        except Exception as exc:
            print(exc)

    def _nuovaproposta(self):
        '''write down a new proposal'''
        len_proposta = self.r.llen('PROPOSTE')
        key_proposta = 'p' + str(len_proposta)
        print('\nINSERT A NEW PROPOSAL (\'ESC\' to exit):')
        while True:
            proposta = input('  New proposal: ')
            if proposta == '':
                print(' INVALID PROPOSAL')
            else:
                break
        if proposta.lower() != 'esc':
            while True:
                proponenti = input('  Proponents: ')
                if proponenti == '':
                    print('  INVALID PROPONENTS')
                else:
                    break
            self.r.hset(key_proposta, 'Titolo', proposta)
            self.r.hset(key_proposta, 'Proponenti', proponenti)
            self.r.rpush('PROPOSTE', key_proposta)
            print('\nPROPOSAL INSERTED CORRECTLY')
        else:
            print('\nRETURN TO MENU')

    def _votoproposta(self):
        '''vote a proposal'''
        all_proposte = self.r.lrange('PROPOSTE', 0, -1)
        print(f'\nLIST OF PROPOSALS OF {self.user}:')
        voti_utente = self.r.smembers('voti:' + self.user)
        if len(voti_utente) == 0:
            for key in all_proposte:
                titolo_proposta = self.r.hget(key, 'Titolo')
                print(f'  {key} -> {titolo_proposta}')
        else:
            for key_p in all_proposte:
                if key_p not in voti_utente:
                    titolo_proposta = self.r.hget(key_p, 'Titolo')
                    print(f'  {key_p} -> {titolo_proposta}')
        print('  x -> Menu')
        while True:
            key_proposta = input('\nTo vote, insert the proposal\'s code: ')
            key_proposta = key_proposta.lower()
            if key_proposta == 'x':
                print('\nRETURN TO MENU')
                break
            elif key_proposta not in voti_utente and key_proposta in all_proposte:
                self.r.zincrby('CLASSIFICA', 1, key_proposta)
                setKey_utente = 'voti:' + self.user
                self.r.sadd(setKey_utente, key_proposta)
                print('\nVOTE CASTED CORRECTLY')
                break
            else:
                print('\nINVALID CODE, TRY AGAIN')

    def _proposteconvoti(self):
        '''print list of voted proposals'''
        print('\nLIST OF VOTED PROPOSALS (by votes)')
        propostewithscore = self.r.zrevrange('CLASSIFICA', 0, -1, withscores=True)
        if len(propostewithscore) == 0:
            print('  NO PROPOSALS')
        else:
            for elem in propostewithscore:
                titolo = self.r.hget(elem[0], 'Titolo')
                print(f'  {titolo}\n    {int(elem[1])} Voti\n')

    def _listaproponenti(self):
        '''print list of proposals + proponents'''
        print('\nLIST OF PROPOSALS AND PROPONENTS:')
        proposte = self.r.lrange('PROPOSTE', 0, -1)
        if len(proposte) == 0:
            print('  NO PROPOSALS')
        else:
            indice = 0
            for elem in proposte:
                indice += 1
                titolo = self.r.hget(elem, 'Titolo')
                proponents = self.r.hget(elem, 'Proponenti')
                print(f'{indice}) {titolo}\n      Proposed by: {proponents}')

    def Menu(self):
        '''menu'''
        self._login()
        print(f'\nUser logged in as: {self.user}')
        while True:
            comando = input(
                (f'\nMenu of {self.user}:'
                 f'\n  1 - Make a proposal'
                 f'\n  2 - Vote a proposal'
                 f'\n  3 - List of proposals and votes'
                 f'\n  4 - List of proposals and proponents'
                 f'\n  0 - Log out'
                 f'\nNumber of desired operation: '))
            try:
                comando = int(comando)
            except:
                print('\nNOT A NUMBER')
            if comando == 0:
                print('\nLOGGED OUT, SEE YOU SOON!')
                break
            elif comando == 1:
                self._nuovaproposta()
            elif comando == 2:
                self._votoproposta()
            elif comando == 3:
                self._proposteconvoti()
            elif comando == 4:
                self._listaproponenti()
            else:
                print('\nINVALID NUMBER, TRY AGAIN')

    def __str__(self):
        return '\nVoting Management System with Redis'


if __name__ == '__main__':
    redis = RedisDB()
    redis.Menu()
